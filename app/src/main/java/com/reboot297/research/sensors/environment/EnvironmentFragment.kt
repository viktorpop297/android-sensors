/*
 * Copyright (c) 2022. Viktor Pop
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.reboot297.research.sensors.environment

import android.hardware.Sensor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.reboot297.research.sensors.BaseSensorFragment
import com.reboot297.research.sensors.R
import com.reboot297.research.sensors.databinding.FragmentEnvironmentBinding
import com.reboot297.research.sensors.motion.MotionFragmentDirections

/**
 * Fragment for environment sensors.
 */
class EnvironmentFragment : BaseSensorFragment<FragmentEnvironmentBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentEnvironmentBinding
        get() = FragmentEnvironmentBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPressureSensor()
        initTemperatureSensor()
        initLightSensor()
        initRelativeHumiditySensor()
    }

    private fun initPressureSensor() {
        val unitPressure = viewModel.metadataMap[Sensor.TYPE_PRESSURE]?.unit?.let { getString(it) }
        initSwitch(binding.pressureView, binding.pressureValueView, Sensor.TYPE_PRESSURE)
        viewModel.sensorsMap[Sensor.TYPE_PRESSURE]?.observe(viewLifecycleOwner) { values ->
            binding.pressureValueView.text =
                getString(R.string.pressure_value, values[0], unitPressure)
        }

        binding.pressureValueView.setOnClickListener {
            findNavController().navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_PRESSURE))
        }
    }

    private fun initTemperatureSensor() {
        initSwitch(
            binding.temperatureView,
            binding.temperatureValueView,
            Sensor.TYPE_AMBIENT_TEMPERATURE
        )
        val unitTemperature =
            viewModel.metadataMap[Sensor.TYPE_AMBIENT_TEMPERATURE]?.unit?.let { getString(it) }
        viewModel.sensorsMap[Sensor.TYPE_AMBIENT_TEMPERATURE]?.observe(viewLifecycleOwner) { values ->
            binding.temperatureValueView.text =
                getString(R.string.ambient_temperature_value, values[0], unitTemperature)
        }

        binding.temperatureValueView.setOnClickListener {
            findNavController()
                .navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_AMBIENT_TEMPERATURE))
        }
    }

    private fun initLightSensor() {
        val unitLight = viewModel.metadataMap[Sensor.TYPE_LIGHT]?.unit?.let { getString(it) }
        initSwitch(binding.lightView, binding.lightValueView, Sensor.TYPE_LIGHT)
        viewModel.sensorsMap[Sensor.TYPE_LIGHT]?.observe(viewLifecycleOwner) { values ->
            binding.lightValueView.text = getString(R.string.light_value, values[0], unitLight)
        }

        binding.lightValueView.setOnClickListener {
            findNavController().navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_LIGHT))
        }
    }

    private fun initRelativeHumiditySensor() {
        initSwitch(
            binding.relativeHumidityView,
            binding.relativeHumidityValueView,
            Sensor.TYPE_RELATIVE_HUMIDITY
        )

        val unitRelativeHumidity =
            viewModel.metadataMap[Sensor.TYPE_RELATIVE_HUMIDITY]?.unit?.let { getString(it) }
        viewModel.sensorsMap[Sensor.TYPE_RELATIVE_HUMIDITY]?.observe(viewLifecycleOwner) { values ->
            binding.relativeHumidityValueView.text =
                getString(R.string.relative_humidity_value, values[0], unitRelativeHumidity)
        }

        binding.relativeHumidityValueView.setOnClickListener {
            findNavController()
                .navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_RELATIVE_HUMIDITY))
        }
    }
}
