/*
 * Copyright (c) 2022. Viktor Pop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.reboot297.research.sensors.calculated

import android.hardware.Sensor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reboot297.research.sensors.R
import com.reboot297.research.sensors.databinding.FragmentCalculatedBinding

/**
 * Fragment for different data that can be calculated.
 */
class CalculatedFragment : BaseCalculatedFragment<FragmentCalculatedBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentCalculatedBinding
        get() = FragmentCalculatedBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAltitude()
        initAbsoluteHumidity()
    }

    private fun initAltitude() {
        val unitPressure = viewModel.metadataMap[Sensor.TYPE_PRESSURE]?.unit?.let { getString(it) }
        initSwitch(binding.altitudeView, binding.pressureValueView, Sensor.TYPE_PRESSURE)
        viewModel.sensorsMap[Sensor.TYPE_PRESSURE]?.observe(viewLifecycleOwner) { values ->
            binding.pressureValueView.text =
                getString(R.string.altitude_pressure_value, values[0], unitPressure)
        }

        val unitAltitude = viewModel.calculatedMetadataMap[TYPE_ALTITUDE]?.unit?.let { getString(it) }
        viewModel.calculatedDataMap[TYPE_ALTITUDE]?.observe(viewLifecycleOwner) { value ->
            binding.altitudeValueView.text =
                getString(R.string.altitude_value, value, unitAltitude)
        }
    }

    private fun initAbsoluteHumidity() {
        initSwitch(
            binding.absoluteHumidityView,
            binding.relativeHumidityValueView,
            Sensor.TYPE_RELATIVE_HUMIDITY,
            Sensor.TYPE_AMBIENT_TEMPERATURE
        )

        val unitRelativeHumidity =
            viewModel.metadataMap[Sensor.TYPE_RELATIVE_HUMIDITY]?.unit?.let { getString(it) }
        viewModel.sensorsMap[Sensor.TYPE_RELATIVE_HUMIDITY]?.observe(viewLifecycleOwner) { values ->
            binding.relativeHumidityValueView.text =
                getString(R.string.calculated_relative_humidity_value, values[0], unitRelativeHumidity)
        }

        val unitTemperature =
            viewModel.metadataMap[Sensor.TYPE_AMBIENT_TEMPERATURE]?.unit?.let { getString(it) }
        viewModel.sensorsMap[Sensor.TYPE_AMBIENT_TEMPERATURE]?.observe(viewLifecycleOwner) { values ->
            binding.temperatureValueView.text =
                getString(R.string.calculated_temperature_value, values[0], unitTemperature)
        }

        val unitAbsoluteHumidity =
            viewModel.calculatedMetadataMap[TYPE_ABSOLUTE_HUMIDITY]?.unit?.let { getString(it) }
        viewModel.calculatedDataMap[TYPE_ABSOLUTE_HUMIDITY]?.observe(viewLifecycleOwner) { value ->
            binding.absoluteHumidityValueView.text =
                getString(R.string.absolute_humidity_value, value, unitAbsoluteHumidity)
        }

        val unitDewPoint =
            viewModel.calculatedMetadataMap[TYPE_DEW_POINT]?.unit?.let { getString(it) }
        viewModel.calculatedDataMap[TYPE_DEW_POINT]?.observe(viewLifecycleOwner) { value ->
            binding.dewPointValueView.text =
                getString(R.string.dew_point_value, value, unitDewPoint)
        }
    }
}
