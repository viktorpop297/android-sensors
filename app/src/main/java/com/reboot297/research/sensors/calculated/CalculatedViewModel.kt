/*
 * Copyright (c) 2022. Viktor Pop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.reboot297.research.sensors.calculated

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager
import androidx.lifecycle.MutableLiveData
import com.reboot297.research.sensors.COUNT_1
import com.reboot297.research.sensors.R
import com.reboot297.research.sensors.SensorMetadata
import com.reboot297.research.sensors.SensorsViewModel
import kotlin.math.exp
import kotlin.math.ln

const val TYPE_ALTITUDE = 1001
const val TYPE_ABSOLUTE_HUMIDITY = 1002
const val TYPE_DEW_POINT = 1003

/**
 * ViewModel for different calculated data.
 */
class CalculatedViewModel : SensorsViewModel() {

    /**
     * Calculated data map.
     */
    val calculatedDataMap = mapOf(
        /**
         * Altitude calculated based on pressure sensor data.
         */
        Pair(TYPE_ALTITUDE, MutableLiveData<Float>()),
        /**
         * Absolute humidity calculated on temperature and relative humidity.
         */
        Pair(TYPE_ABSOLUTE_HUMIDITY, MutableLiveData<Float>()),
        /**
         * Dew point calculated on temperature and relative humidity.
         */
        Pair(TYPE_DEW_POINT, MutableLiveData<Float>())
    )

    val calculatedMetadataMap = mapOf(
        Pair(
            TYPE_ALTITUDE,
            SensorMetadata(R.string.unit_meter, COUNT_1, R.string.description_altimeter)
        ),
        Pair(
            TYPE_ABSOLUTE_HUMIDITY,
            SensorMetadata(
                R.string.unit_grams_per_meter_cubic,
                COUNT_1,
                R.string.description_absolute_humidity
            )
        ),
        Pair(
            TYPE_DEW_POINT,
            SensorMetadata(
                R.string.unit_celcius,
                COUNT_1,
                R.string.description_dew_point
            )
        )
    )

    override fun onSensorChanged(event: SensorEvent?) {
        super.onSensorChanged(event)

        when (event?.sensor?.type) {
            Sensor.TYPE_PRESSURE -> updateAltitude(event)
            Sensor.TYPE_RELATIVE_HUMIDITY,
            Sensor.TYPE_AMBIENT_TEMPERATURE -> {
                val temperature: Float =
                    sensorsMap[Sensor.TYPE_AMBIENT_TEMPERATURE]?.value?.let { it -> it[0] } ?: 0f
                val relativeHumidity: Float =
                    sensorsMap[Sensor.TYPE_RELATIVE_HUMIDITY]?.value?.let { it -> it[0] } ?: 0f
                updateAbsoluteHumidity(temperature, relativeHumidity)
                updateDewPoint(temperature, relativeHumidity)
            }
        }
    }

    private fun updateAltitude(event: SensorEvent) {
        event.values?.let { values ->
            val altitude =
                SensorManager.getAltitude(
                    SensorManager.PRESSURE_STANDARD_ATMOSPHERE,
                    values[0]
                )
            calculatedDataMap[TYPE_ALTITUDE]?.postValue(altitude)
        }
    }

    private fun updateAbsoluteHumidity(temperature: Float, relativeHumidity: Float) {
        val absoluteHumidity: Float = calculateAbsoluteHumidity(temperature, relativeHumidity)
        calculatedDataMap[TYPE_ABSOLUTE_HUMIDITY]?.postValue(absoluteHumidity)
    }

    private fun updateDewPoint(temperature: Float, relativeHumidity: Float) {
        val dewPoint = calculateDewPoint(temperature, relativeHumidity)
        calculatedDataMap[TYPE_DEW_POINT]?.postValue(dewPoint)
    }

    companion object {

        /**
         * Mass constant.
         */
        private const val M = 17.62f

        /**
         * Pressure constant. in hP.
         */
        private const val A = 6.112f

        /**
         * Temperature constant for converting in kelvin.
         */
        private const val K = 273.15f

        /**
         * Temperature constant.
         */
        private const val Tn = 243.12f

        /**
         * Temperature constant.
         */
        private const val Ta = 216.7f

        /**
         * Calculate Absolute humidity.
         *
         * @param temperature actual  temperature in degrees C
         * @param relativeHumidity relative humidity in percent
         *
         * @return absolute humidity in grams/meter3
         */
        @Suppress("MagicNumber")
        private fun calculateAbsoluteHumidity(temperature: Float, relativeHumidity: Float): Float {
            return (Ta * (relativeHumidity / 100) * A * exp(M * temperature / (Tn + temperature)) / (K + temperature))
        }

        /**
         * Calculate dew point temperature.
         *
         * @param t actual  temperature in degrees C
         * @param rH relative humidity in percent
         *
         * @return dew point temperature in degrees C
         */
        @Suppress("MagicNumber")
        private fun calculateDewPoint(t: Float, rH: Float): Float {
            return (Tn * ((ln(rH / 100) + M * t / (Tn + t)) / (M - (ln(rH / 100) + M * t / (Tn + t)))))
        }
    }
}
