/*
 * Copyright (c) 2022. Viktor Pop
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.reboot297.research.sensors

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.viewbinding.ViewBinding

/**
 * Base Fragment for sensors.
 */
abstract class BaseSensorFragment<VB : ViewBinding> : Fragment() {

    private var _binding: ViewBinding? = null
    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> VB

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    val viewModel: SensorsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = bindingInflater.invoke(inflater, container, false)
        return requireNotNull(_binding).root
    }

    fun initSwitch(switch: SwitchCompat, textView: TextView, sensorType: Int) {
        switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                val isAvailable = viewModel.startTracking(sensorType)
                if (!isAvailable) {
                    textView.text = getString(R.string.sensor_not_available)
                }
            } else {
                viewModel.stopTracking(sensorType)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.stopTracking()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
