/*
 * Copyright (c) 2022. Viktor Pop
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.reboot297.research.sensors

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.reboot297.research.sensors.databinding.FragmentDetailsBinding

/**
 * Fragment for sensor details.
 */
class DetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null
    private val binding: FragmentDetailsBinding
        get() = _binding as FragmentDetailsBinding

    private val args: DetailsFragmentArgs by navArgs()
    private val viewModel: SensorsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSensor()
    }

    private fun initSensor() {

        val sensorType = args.sensorType
        viewModel.metadataMap[sensorType]?.description
            ?.let { binding.descriptionView.setText(it) }

        val unit = viewModel.metadataMap[sensorType]?.unit
            ?.let { getString(it) }
        val count = viewModel.metadataMap[sensorType]?.countValues
        viewModel.sensorsMap[sensorType]?.observe(viewLifecycleOwner) { values ->
            if (count == COUNT_1) {
                updateSingleValue(unit, values[0])
            } else if (count == COUNT_3) {
                updateValue(unit, values)
            }
        }
        updateSensorInfo(sensorType)
    }

    private fun updateSingleValue(unit: String?, value: Float) {
        binding.valueXView.text = getString(R.string.sensor_one_value, value, unit)
    }

    private fun updateValue(unit: String?, values: FloatArray) {
        binding.valueXView.text =
            getString(R.string.sensor_axis_value, "X", values[0], unit)
        binding.valueYView.text =
            getString(R.string.sensor_axis_value, "Y", values[1], unit)
        binding.valueZView.text =
            getString(R.string.sensor_axis_value, "Z", values[2], unit)
    }

    private fun updateSensorInfo(sensorType: Int) {
        binding.infoTypeValueView.text = viewModel.getSensorInfo(sensorType)?.stringType
        binding.infoTypeIdValueView.text = viewModel.getSensorInfo(sensorType)?.type.toString()
        binding.infoNameValueView.text = viewModel.getSensorInfo(sensorType)?.name
        binding.infoVendorValueView.text = viewModel.getSensorInfo(sensorType)?.vendor
        binding.infoVersionValueView.text = viewModel.getSensorInfo(sensorType)?.version.toString()
        binding.infoMaxRangeValueView.text =
            viewModel.getSensorInfo(sensorType)?.maximumRange.toString()
        binding.infoResolutionValueView.text =
            viewModel.getSensorInfo(sensorType)?.resolution.toString()
        binding.infoPowerValueView.text = viewModel.getSensorInfo(sensorType)?.power.toString()
        binding.infoMinDelayValueView.text =
            viewModel.getSensorInfo(sensorType)?.minDelay.toString()
        binding.infoMaxDelayValueView.text =
            viewModel.getSensorInfo(sensorType)?.maxDelay.toString()
        viewModel.metadataMap[sensorType]?.unit?.let {
            binding.infoUnitValueView.setText(it)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
