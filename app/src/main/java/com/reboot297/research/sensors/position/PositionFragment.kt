/*
 * Copyright (c) 2022. Viktor Pop
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.reboot297.research.sensors.position

import android.hardware.Sensor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.reboot297.research.sensors.BaseSensorFragment
import com.reboot297.research.sensors.R
import com.reboot297.research.sensors.databinding.FragmentPositionBinding
import com.reboot297.research.sensors.motion.MotionFragmentDirections

/**
 * Fragment for position sensors.
 */
class PositionFragment : BaseSensorFragment<FragmentPositionBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentPositionBinding
        get() = FragmentPositionBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProximitySensor()
        initGrvSensor()
        initMagneticFieldSensor()
    }

    private fun initProximitySensor() {
        initSwitch(
            binding.proximityView,
            binding.proximityValueView,
            Sensor.TYPE_PROXIMITY
        )

        val unit = viewModel.metadataMap[Sensor.TYPE_PROXIMITY]?.unit?.let { getString(it) }

        viewModel.sensorsMap[Sensor.TYPE_PROXIMITY]?.observe(viewLifecycleOwner) { values ->
            binding.proximityValueView.text = getString(R.string.proximity_value, values[0], unit)
        }
        binding.proximityValueView.setOnClickListener {
            findNavController().navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_PROXIMITY))
        }
    }

    private fun initGrvSensor() {
        initSwitch(
            binding.geomagneticRotationVectorView,
            binding.geomagneticRotationVectorXValueView,
            Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR
        )

        viewModel.sensorsMap[Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR]?.observe(viewLifecycleOwner) { values ->
            binding.geomagneticRotationVectorXValueView.text =
                getString(R.string.geomagnetic_rotation_vector_value, "X", values[0])
            binding.geomagneticRotationVectorYValueView.text =
                getString(R.string.geomagnetic_rotation_vector_value, "Y", values[1])
            binding.geomagneticRotationVectorZValueView.text =
                getString(R.string.geomagnetic_rotation_vector_value, "Z", values[2])
        }

        binding.geomagneticRotationVectorXValueView.setOnClickListener {
            findNavController()
                .navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR))
        }
    }

    private fun initMagneticFieldSensor() {
        initSwitch(
            binding.magneticFieldView,
            binding.magneticFieldXValueView,
            Sensor.TYPE_MAGNETIC_FIELD
        )

        val unit = viewModel.metadataMap[Sensor.TYPE_MAGNETIC_FIELD]?.unit?.let { getString(it) }
        viewModel.sensorsMap[Sensor.TYPE_MAGNETIC_FIELD]?.observe(viewLifecycleOwner) { values ->
            binding.magneticFieldXValueView.text =
                getString(R.string.magnetic_field_value, "X", values[0], unit)
            binding.magneticFieldYValueView.text =
                getString(R.string.magnetic_field_value, "Y", values[1], unit)
            binding.magneticFieldZValueView.text =
                getString(R.string.magnetic_field_value, "Z", values[2], unit)
        }

        binding.magneticFieldXValueView.setOnClickListener {
            findNavController()
                .navigate(MotionFragmentDirections.actionToDetailsFragment(Sensor.TYPE_MAGNETIC_FIELD))
        }
    }
}
